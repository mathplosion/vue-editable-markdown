### **Solution**

$$
\LARGE s = x + \mu =`r x`+`r mu`=`r s`
$$

$$
\LARGE d = x - \mu =`r x`-`r mu`=`r d`
$$

Here is some text based inline: x=`r x`, and 2(x)=`r 2*x`

```{r plot1}
renderPlot({
    X<-rnorm(mu_d())
    hist(X)
})
```
