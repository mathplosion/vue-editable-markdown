library(stringi)

#pass it one line
process_inlines<-function(line) {
	#pat<-"`r[ #]([^`]+)\\s*`"
    pat<-"(?<!(^|\n)``)`r[ #]([^`]+)`"

    has_inline<-stri_detect(line, regex=pat)
	if(has_inline) {
        loc <- stri_locate_all(line, regex=pat)[[1L]]
        groups <- stri_match_all(line, regex=pat)[[1L]] 
        #print(groups)

		inline_r_exprs<-unlist(lapply(groups[,3], function(group) {
				inline<-paste0("{{", group, "}}")
				return (inline)
				}))

		start<-loc[,1]
		end<-loc[,2]
		line<-stri_sub_replace_all(line, 
                                   start, 
                                   end, 
					       replacement=inline_r_exprs)
	}
    return (line)
}



test<-"This is some lines with inline `r a*b` And this is some more `r a-b`"
newline<-process_inlines(test)
print(newline)
