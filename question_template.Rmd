
### **Question**
We have this:

$$\LARGE
\begin{align*}
    x &= `r x`  &  \mu &=`r mu`
\end{align*}
$$

Then find the $z$-value.

```{r plot1, eval=T}
renderPlot({
    X<-rnorm(mu_d())
    Y<-rnorm(mu_d())
    plot(x=X,y=Y, pch=19)
})
```
