#!/usr/bin/env Rscript

library(parsermd)
library(argparser)
library(rlang)
library(stringi)

# This is applied to params or computed code block, passes in reactive vars
create_reactive_code<-function(lines, vars) {
    lines<-unlist(lapply(lines, function(line) {
       parsed<-parse_expr(line)
       parts<-get_lhs_rhs(parsed)

       lhs<-parts[1]
       rhs<-parts[2]

       rhs<-gsub("params\\$","input\\$", rhs)
       
       if(!is.null(vars)) { 
           rhs<-make_line_reactive(rhs, vars)
       }
       line<-paste0(lhs, "<-", "reactive({", rhs, "})")

       return (line)
    })) 
    return (lines)
}

# Given lines from code block, look through and 
# find the lhs's and add them to list of reactive vars
create_reactive_vars<-function(lines) {
    vars<-unlist(lapply(lines, function(line) {
       parsed<-parse_expr(line)
       parts<-get_lhs_rhs(parsed)
       return (parts$lhs)
    }))
    return (vars)
}

get_chunk_code <- function(rmd_env, name) {
    index<-get_list_index_for_chunk(rmd_env,name)
    lines<-(rmd_env$rmd)[[index]]$code
}

set_code_chunk<-function(rmd_env, label, lines) {
    index<-get_list_index_for_chunk(rmd_env, label)
    rmd_env$rmd[[index]]$code<-lines
} 

# returns reactive code for renderXXX code chunks 
do_renderXXX_code<-function(rmd_env, name, func, vars) {
    index<-get_list_index_for_chunk(rmd_env, name)
    if(index > 0) {
        lines<-rmd_env$rmd[[index]]$code

        lines<-unlist(lapply(lines, function(line) {
           line<-make_line_reactive(line, vars)
           line<-paste0("    ", line)
           return (line)
        })) 

        chunk<-c(paste0(func,"({"), lines, "})")
        set_code_chunk(rmd_env, name, chunk)
    } 
}

# get symbols in code that are included on vars list
get_symbols_df<-function(code, vars) {
    options("keep.source"=TRUE)
    parse_df<-getParseData(parse(text=code), includeText=TRUE)
    symbols_df=parse_df[which(parse_df$token=="SYMBOL"),]

    # Restrict to symbols in the vars list
    newsymbols_df<-symbols_df[symbols_df$text %in% vars,]
    return (newsymbols_df)
}

# make a line reactive by finding symbols (on vars list) and 
# turning into functions 
make_line_reactive<-function(line, vars) {

    symbols_df<-get_symbols_df(line, vars)
    symbols<-symbols_df$text
    replacement<-paste0(symbols, "()")
    start<-symbols_df$col1
    end<-symbols_df$col2

    new_line<-stri_sub_replace_all(line, 
                                   start, 
                                   end, 
                                   replacement=replacement)
    return (new_line)
}

get_lhs_rhs<-function(expr) {
    # should check this is an assignment
	lhs<-deparse(expr[[2]])
	rhs<-deparse(expr[[3]])
    parts<-c()
    parts$lhs<-lhs
    parts$rhs<-rhs
    return (parts) 
}

get_list_index_for_chunk<-function(rmd_env, label) {
    index<-which(sapply(rmd_env$rmd, function(item){
        class(item) =="rmd_chunk" && item$name == label 
    }))

    if(identical(index, integer(0)))
        index<- -1

    return (index)
}

# This is where we parse the Rmd and process it..

p <- arg_parser("Convert problem Rmd to an (runtime: shiny) editable problem Rmd")

# Add command line arguments
p <- add_argument(p, "input", help="input Rmd file")
p <- add_argument(p, "output", help="output Rmd file ")

# Parse the command line arguments
argv <- parse_args(p)

# Parse the Rmd
rmd_env<-new.env()
rmd_env$rmd = parse_rmd(argv$input)
rmd_env$rmd[[1]]$runtime<-"shiny"

# Params code chunk
lines<-get_chunk_code(rmd_env, "params")
param_vars<-create_reactive_vars(lines) 
print(param_vars)
code<-create_reactive_code(lines, NULL)
print(code)
set_code_chunk(rmd_env, "params", code)

# Computed code chunk 
lines<-get_chunk_code(rmd_env, "computed")
computed_vars<-create_reactive_vars(lines)
print(computed_vars)
reactive_vars<-c(param_vars, computed_vars) 
code<-create_reactive_code(lines, reactive_vars)
print(code)
set_code_chunk(rmd_env, "computed", code)

# Do renderXXX code chunks 
print("1")
do_renderXXX_code(rmd_env, "plot", "renderPlot", reactive_vars)
print("2")

as_document(rmd_env$rmd, collapse="\n") %>%
    cat(file=argv$output, sep="\n")

# as_document(rmd, collapse="\n") %>%
#     cat(sep="\n")
