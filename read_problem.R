library(katex)
library(yaml)
library(parsermd)
library(rlang)
library(stringi)

# This is applied to params or computed code block, passes in reactive vars
create_reactive_code<-function(lines, vars) {
    lines<-unlist(lapply(lines, function(line) {
       parsed<-parse_expr(line)
       parts<-get_lhs_rhs(parsed)

       lhs<-parts[1]
       rhs<-parts[2]

       rhs<-gsub("params\\$","input\\$", rhs)
       
       if(!is.null(vars)) { 
           rhs<-make_line_reactive(rhs, vars)
       }
       line<-paste0(lhs, "<-", "reactive({", rhs, "})")

       return (line)
    })) 
    return (lines)
}

# Given lines from code block, look through and 
# find the lhs's and add them to list of reactive vars
create_reactive_vars<-function(lines) {
    vars<-unlist(lapply(lines, function(line) {
       parsed<-parse_expr(line)
       parts<-get_lhs_rhs(parsed)
       return (parts$lhs)
    }))
    return (vars)
}

get_chunk_code <- function(rmd_env, name, which_rmd="rmd") {
    index<-get_list_index_for_chunk(rmd_env[[which_rmd]], name)
    lines<-rmd_env[[which_rmd]][[index]]$code
    return (lines)
}

set_code_chunk<-function(rmd_env, label, lines, which_rmd="rmd") {
    index<-get_list_index_for_chunk(rmd_env[[which_rmd]], label)
    rmd_env[[which_rmd]][[index]]$code<-lines
} 

make_lines_reactive<-function(lines, vars) {
    newlines<-unlist(lapply(lines, function(line) {
       line<-make_line_reactive(line, vars)
       line<-paste0("    ", line)
       return (line)
    })) 

    return (newlines)
}

# returns reactive code for renderXXX code chunks 
do_renderXXX_code<-function(rmd_env, chunkname, func, vars) {

    index<-get_list_index_for_chunk(rmd_env$question, chunkname)
    if(!identical(index, integer(0))) {
        lines<-rmd_env$question[[index]]$code
        newlines <-make_lines_reactive(lines, vars)
        chunk<-c(paste0(func,"({"), newlines, "})")
        set_code_chunk(rmd_env, chunkname, chunk, which_rmd="question")
    }

    index<-get_list_index_for_chunk(rmd_env$answer, chunkname)
    if(!identical(index, integer(0))) {
        lines<-rmd_env$answer[[index]]$code
        newlines <-make_lines_reactive(lines, vars)
        chunk<-c(paste0(func,"({"), newlines, "})")
        set_code_chunk(rmd_env, chunkname, chunk, which_rmd="answer")
    }
}

# get symbols in code that are included on vars list
get_symbols_df<-function(code, vars) {
    options("keep.source"=TRUE)
    parse_df<-getParseData(parse(text=code), includeText=TRUE)
    symbols_df=parse_df[which(parse_df$token=="SYMBOL"),]

    # Restrict to symbols in the vars list
    newsymbols_df<-symbols_df[symbols_df$text %in% vars,]
    return (newsymbols_df)
}

# make a line reactive by finding symbols (on vars list) and 
# turning into functions 
make_line_reactive<-function(line, vars) {

    symbols_df<-get_symbols_df(line, vars)
    symbols<-symbols_df$text
    replacement<-paste0(symbols, "()")
    start<-symbols_df$col1
    end<-symbols_df$col2

    new_line<-stri_sub_replace_all(line, 
                                   start, 
                                   end, 
                                   replacement=replacement)
    return (new_line)
}

get_lhs_rhs<-function(expr) {
    # should check this is an assignment
	lhs<-deparse(expr[[2]])
	rhs<-deparse(expr[[3]])
    parts<-c()
    parts$lhs<-lhs
    parts$rhs<-rhs
    return (parts) 
}

get_list_index_for_chunk<-function(rmd, label) {

    hasLabel<-sapply(rmd, function(item){
        class(item) =="rmd_chunk" && item$name == label 
    })

    index<-which(hasLabel)
    return (index)
}

# Parse the Rmd, put in an env so we can pass by reference
rmd_env<-new.env()
rmd_env$rmd<-parse_rmd("problem.Rmd")
rmd_env$rmd[[1]]$runtime<-"shiny"

# Params code chunk
param_lines<-get_chunk_code(rmd_env, "params")
param_vars<-create_reactive_vars(param_lines) 
param_code<-create_reactive_code(param_lines, NULL)
set_code_chunk(rmd_env, "params", param_code)

# Computed code chunk 
computed_lines<-get_chunk_code(rmd_env, "computed")
computed_vars<-create_reactive_vars(computed_lines)
reactive_vars<-c(param_vars, computed_vars) 
computed_code<-create_reactive_code(computed_lines, reactive_vars)
print(reactive_vars)
set_code_chunk(rmd_env, "computed", computed_code)

q<-get_list_index_for_chunk(rmd_env$rmd, "question")
print(rmd_env$rmd[[q]]$options$child)
q_filename<-rmd_env$rmd[[q]]$options$child
q_filename<-substring(q_filename, 2, nchar(q_filename)-1)

a<-get_list_index_for_chunk(rmd_env$rmd, "answer")
print(rmd_env$rmd[[a]]$options$child)
a_filename<-rmd_env$rmd[[a]]$options$child
a_filename<-substring(a_filename, 2, nchar(a_filename)-1)

print(q_filename)
print(a_filename)

rmd_env$question<-parse_rmd(q_filename)
rmd_env$answer<-parse_rmd(a_filename)

print(str(rmd_env$question))
print(str(rmd_env$answer))

# Do renderXXX code chunks 
do_renderXXX_code(rmd_env, "plot", "renderPlot", reactive_vars)

rmd_env$rmd[[a]]$options$child<-"'./answerShiny.Rmd'"
rmd_env$rmd[[q]]$options$child<-"'./questionShiny.Rmd'"

as_document(rmd_env$rmd, collapse="\n") %>%
cat(file="problemShiny.Rmd", sep="\n")

as_document(rmd_env$question, collapse="\n") %>%
cat(file="questionShiny.Rmd", sep="\n")

as_document(rmd_env$answer, collapse="\n") %>%
cat(file="answerShiny.Rmd", sep="\n")

