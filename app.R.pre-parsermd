library(katex)
library(yaml)

chunk.begin <- '^[\t >]*```+\\s*\\{([a-zA-Z0-9_]+( *[ ,].*)?)\\}\\s*$'
chunk.end <- '^[\t >]*```+\\s*$'

# create new rmdlines 
create_new_rmdlines<-function(rmdlines, 
                              new_params_lines, 
                              new_computed_lines) {

    begins <- grep(chunk.begin, rmdlines)
    ends <- grep(chunk.end, rmdlines)

    beforeParams<-begins[1]
    afterParams<-ends[1]

    beforeCompute<-begins[2]
    afterCompute<-ends[2]

    newrmdlines<-c(rmdlines[1:beforeParams], 
                   new_params_lines,
                   rmdlines[afterParams:beforeCompute],
                   new_computed_lines,
                   rmdlines[afterCompute:length(rmdlines)])

    return (newrmdlines)
}

get_param_lines<-function(rmdlines) {
    begins <- grep(chunk.begin, rmdlines)
    ends <- grep(chunk.end, rmdlines)

    paramsStart<-begins[1]+1
    paramsEnd<-ends[1]-1
    return (rmdlines[paramsStart:paramsEnd])
}

get_computed_lines<-function(rmdlines) {
    begins <- grep(chunk.begin, rmdlines)
    ends <- grep(chunk.end, rmdlines)

    computedStart<-begins[2]+1
    computedEnd<-ends[2]-1
    return (rmdlines[computedStart:computedEnd])
}

split_assignment<-function(line) {
    split<-strsplit(line, "<-")
    l<-split[[1]][1]
    r<-split[[1]][2]
    l<-trimws(l)
    r<-trimws(r)
    v<-c(l,r)
    return (v)
}

create_computed_lines<-function(lines, 
                                params_assignments) {

    sides<-get_left_right_sides(lines)
    lhs<-sides[[1]]
    rhs<-sides[[2]]

    newlines<-c()

    for(i in 1:length(lhs)) {
        if(i > 1)
            prev_lhs<-lhs[1:(i-1)]
        else
            prev_lhs<-c()

        assignments<-""
        if(length(prev_lhs) > 0) {
            for(j in 1:length(prev_lhs)) {
                name<-prev_lhs[j]
                assignment<-paste0(name, "<-", name, "()", ";")
                assignments<-paste0(assignments, assignment)
            }
        }

        line<-paste0(lhs[i], "<-", "reactive({", 
                                        params_assignments,
                                        assignments, 
                                        rhs[i], 
                                    "})")
        newlines<-c(newlines, line)

    }
    return (newlines)
}

get_left_right_sides<-function(lines){
    lhs<-c()
    rhs<-c()
    for(i in 1:length(lines)) {
        parts<-split_assignment(lines[i])
        lhs[i]<-parts[1]
        rhs[i]<-parts[2]
    }

    return (list(lhs, rhs))
}

create_params_lines<-function(lines) {
    newlines<-c()
    for(i in 1:length(lines)) {
        parts<-split_assignment(lines[i])
        lhs<-parts[1]
        rhs<-parts[2]
        newline<-paste0(lhs, "<-", "reactive(", "input$", lhs, ")")    
        newlines<-c(newlines, newline)
    }
  return (newlines)
}

get_params_names<-function(lines) {
    params_names<-c()
    for(i in 1:length(lines)) {
        parts<-split_assignment(lines[i])
        params_names<-c(params_names, parts[1])
    }
  return (params_names)
}

get_computed_names<-function(lines) {
    computed_names<-c()
    for(i in 1:length(lines)) {
        parts<-split_assignment(lines[i])
        computed_names<-c(computed_names, parts[1])
    }
  return (computed_names)
}

make_assignments<-function(names) {
    assignments<-sapply(names, function(name) { 
        assignment<-paste0(name, "<-", name, "();")
    })

  # returns something like "a<-a(); b<-b();"
  return (paste0(assignments, collapse=""))
}

make_function_calls<-function(names) {
    function_calls<-sapply(names, function(name) { 
        function_call<-paste0(name, "()")
    })

  # returns something like "a(); b();"
  return (paste0(function_calls, collapse=";"))
}

# pass this a named vector of values
# like "x:3, mu:6"
make_js_object_fields<-function(values){

  object_fields<-sapply(names(values), function(name) {
     return (paste0(name,":", values[[name]]))
  })
  return(paste0(object_fields, collapse=","))
}

# combining vector of lines into one text string
combine_lines<-function(lines) {
  return (paste0(lines, collapse="\n"))
}

# split text with new lines into vector of lines
split_lines<-function(text) {
  return(strsplit(text,"\n")[[1]])
}

split_file<-function(lines) {
    delims <- grep("^(---|\\.\\.\\.)\\s*$", lines)
    firstLineOfYaml<-delims[1]+1
    lastLineOfYaml<-delims[2]-1
    firstLineOfBody<-delims[2]+1
    lastLineOfBody<-length(lines)
    yaml_lines<-lines[firstLineOfYaml:lastLineOfYaml]
    body_lines<-lines[firstLineOfBody:lastLineOfBody]
    return(list(yaml_lines, body_lines))
}

combine_parts_into_lines<-function(yaml_lines, body_lines) {
    filelines<-c("---", yaml_lines, "---", body_lines) 
    return (filelines)
}

# This is a slider that does not delay... 
immediateSliderInput<- function(...) {
   sld <- sliderInput(...)
   sld$children[[2]]$attribs$`data-immediate` <- "true"
   sld
}

ui <- htmlTemplate("index.html")

server <- function(input, output, session) {

  cat("                                \n")
  cat("        starting server          \n")
  cat("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n")
  cat("                                \n")

  computedObserver<-NULL
  firstTime<-TRUE

  yaml_object<-reactiveVal()
  rmd_body_lines<-reactiveVal()

  filename<-"chunks.Rmd"
  text<-readLines(filename)
  writeLines(text, "Temp.Rmd")

  renderCount<-reactiveVal(0)
  reactiveFile<-reactiveFileReader(100, 
                                   session, 
                                   "Temp.Rmd", 
                                   readFunc=readLines)

  cat("creating rmdeditor output\n")
  output$rmdeditor<-renderUI({
    cat("renderUI for rmdeditor called\n")
    lines<-isolate(reactiveFile())

    len<-length(lines)
    text<-combine_lines(lines)
    textAreaInput("rmd", "", 
                  width = "100%", 
                  height = "100%", 
                  rows = len, 
                  value=text)
             })

  observeEvent(input$mytabset, {
    cat("input$mytabset is: ", input$mytabset, "\n")
    if(input$mytabset == "Editor") {
        cat("Just switched to editor " ,firstTime, "\n")

       yaml<-isolate(yaml_object())
	   for(name in names(yaml$params)) { 
           cat("input$", name,  ":", input[[name]], "\n", sep="")
           yaml$params[[name]]$value<-input[[name]]
	   }

       yaml_object(yaml)

       yaml_text<-as.yaml(isolate(yaml_object()))

	   yaml_lines<-split_lines(yaml_text)
       rmd_lines<-combine_parts_into_lines(yaml_lines, isolate(rmd_body_lines()))

       rmd_text<-combine_lines(rmd_lines)
       updateTextAreaInput(session, "rmd", value=rmd_text)
    }

    if(input$mytabset == "Preview") {
        cat("firstTime is " ,firstTime, "\n")
        if(!firstTime) {
	        writeLines(isolate(input$rmd), "Temp.Rmd")
        } else {
            firstTime <<- FALSE
        }
     }
  })

  observeEvent(reactiveFile(), {
      cat("reactiveFile changed\n")

      lines<-reactiveFile()
      parts<-split_file(lines)

      yaml_lines<-parts[[1]]
      body_lines<-parts[[2]]

      yaml_text<-combine_lines(yaml_lines)
      yaml<-yaml.load(yaml_text)

      yaml_object(yaml)
      rmd_body_lines(body_lines)

      # Create the param components for paramsui
      components<-lapply(names(yaml$params), function(name) {
            param<-yaml$params[[name]]
            latexLabel<-paste0("\\Large ", param$label)
            katexLabel<-HTML(katex_html(latexLabel, displayMode=F))
            switch(param$input, 
                "slider" = immediateSliderInput(name, 
                            label = katexLabel,
                            min = param$min, 
                            max = param$max, 
                            step = param$step, 
                            value = param$value, 
                            width="100%"),
                "numeric" = numericInput(name, 
                            label = katexLabel,
                            value = param$value, 
                            width="100%")
            )
      })

      cat("creating paramsui output\n")
      output$paramsui<-renderUI({
            cat("renderUI for paramsui called\n")
            tagList(components)
          }
      )

      yaml<-isolate(yaml_object()) 
      inputs<-sapply(names(yaml$params), function(name) {
         return (paste0("input$",name))
      })

      str(inputs)

      # This will be like this: "input$x, input$mu"
      inputsVectorString<-paste0(inputs, collapse=",")

      # Now this: "c(input$x, input$mu)"
      inputsVectorString<-paste0("c(", inputsVectorString, ")")

      cat(inputsVectorString, "\n")

      cat("creating observer for inputs... runs once\n")
      observeEvent({
        #evaling something like this: "c(input$x,input$mu)"
        eval(parse(text=inputsVectorString))
      }, {
           cat("inputs changed\n")
           for(name in names(yaml$params)) 
              cat("input$", name,  ":", input[[name]], sep="", "\n")
           renderCount(renderCount()+1)
      }, once=TRUE)

      # Render the problem
      cat("creating vue output \n")
      output$vue <- renderUI({
      	  cat("renderUI for vue called\n")
          req(renderCount()>0)

          cat("Doing Render...RenderCount is: \n")
          cat(renderCount(), "\n")

          rmdlines<-readLines("Temp.Rmd")

          params_lines<-get_param_lines(rmdlines)
          param_names<-get_params_names(params_lines)

          params_assignments<-make_assignments(param_names)
          new_params_lines<-create_params_lines(params_lines)

          computed_lines<-get_computed_lines(rmdlines)
          computed_names<-get_computed_names(computed_lines)
          new_computed_lines<-create_computed_lines(computed_lines, params_assignments)

          print(param_names)
          print(computed_names)
          print(params_assignments)

          newrmdlines<-create_new_rmdlines(rmdlines, 
                                        new_params_lines, 
                                        new_computed_lines)

          cat(newrmdlines, file="TempShiny.Rmd", sep="\n")

        # Leave out the inline.code and knitr just ignores it...
          myRmdPat<-list(
                chunk.begin = '^[\t >]*```+\\s*\\{([a-zA-Z0-9_]+( *[ ,].*)?)\\}\\s*$',
                chunk.end = '^[\t >]*```+\\s*$',
                ref.chunk = '^\\s*<<(.+)>>\\s*$'
          )
          knitr::knit_patterns$set(myRmdPat)  
          rmarkdown::render("TempShiny.Rmd", 
                            output_format="html_fragment", 
                            quiet=T, 
                            clean=FALSE, 
                            envir=environment());


          if(!is.null(computedObserver)) {
              computedObserver$destroy()
          }

          computedObserver<<-observeEvent({ 
                  sapply(computed_names, function(name) {
                    do.call(name, args=list())  
                  }) 
             }, {
                  sapply(computed_names, function(name) {
                      val<-do.call(name, args=list())
                      session$sendCustomMessage(name, val)
                  }) 
	         })

          customMessageHandlers<-sapply(computed_names, function(name) {
               js<-paste0("Shiny.addCustomMessageHandler('", 
                          name, "',function(val){vm.$data[['", 
                          name, "']]=val})")
               return (js)
          })
          customMessageHandlers<-paste0(customMessageHandlers, collapse=";")

          yaml<-isolate(yaml_object()) 

          # Make the string "x:32, mu:45" using input$x and input$mu
          input_values<-sapply(names(yaml$params), function(name) {
               return (isolate(input[[name]]))
          })
          names(input_values)<-names(yaml$params)
          inputData<-make_js_object_fields(input_values)

          # Make the string "s:77, d:-13" using s() and d() 
          computed_values<-sapply(computed_names, function(name) {
               return (isolate(do.call(name, args=list())))
          })
          names(computed_values)<-computed_names
          computedData<-make_js_object_fields(computed_values)

          # Make the string "x:32, mu:45, s:77, d:-13" 
	      vueData<-paste0(inputData, ",",  computedData)

          cat(vueData, "\n")

          problemlines<-readLines("TempShiny.html")
          problemhtml<-paste(problemlines, collapse="\n")

          vuelines<-readLines("vue-template.html")
          vuetemplate<-paste(vuelines, collapse="\n")
          vuehtml<-knitr::knit_expand(text=vuetemplate, 
                                      problemhtml=problemhtml, 
                                      customMessageHandler=customMessageHandlers,
                                      vuedata=vueData,
                                      delim=c("{%","%}"))

          HTML(vuehtml)
      })
  })
}

shinyApp(ui = ui, server = server)
